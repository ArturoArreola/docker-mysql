**Crear un contenedor con una base de datos MySQL**

1. Descargar el docker image con la imagen de MySQL
	
	`$ docker pull mysql`

2.  Crear el contenedor con la imagen descargada (tomando en cuenta los parámetros para conectarte remotante a través de algún puerto de localhost)

`$ docker run -d -p 3333:3306 --name dockermysql -e "MYSQL_ROOT_PASSWORD=123456" -e "MYSQL_DATABASE=facturas" -e "MYSQL_USER=facturasuser" -e "MYSQL_PASSWORD=123456"  mysql:latest`

> *P.E. docker run
>  			-d (para que inicie en el background)
>  			-p [puerto en el que queremos que corra en nuestro localhost]:3306 
>  			--name [nombre del contenedor] 
>  			-e "MYSQL_ROOT_PASSWORD=[contraseña para el root]" 
>  			-e "MYSQL_DATABASE=[nombre de la base de datos]" 
>  			-e "MYSQL_USER=[nombre del usuario con permisos en la base de datos creada]" 
>  			-e "MYSQL_PASSWORD=[contraseña del usuario creado]"  mysql:[tag para la version de la imagen]*


3.  Verificar que el contenedor se haya creado correctamente

	`$ docker ps`

4.  Se puede ver el log de la creación del contenedor con el comando 

	`$ docker logs -f dockermysql`
	

> *docker logs -f [nombre del contenedor]* 

5.  Conectarnos localmente con el MySQL del contenedor a través del puerto seleccionado

`$ mysql -u facturasuser -p123456 -h 127.0.0.1 --database=facturas --port 3333`

 
> *mysql -u [usuario] -p[contaseña del usuario] -h [IP Localhost] --database=[nombre de la base de datos] --port [puerto seleccionado al momento de la creación del contenedor]* 

6.  En caso de que se apague el equipo, se debe volver a iniciar el contenedor con el siguiente comando

`$ docker start dockermysql`

> *docker start [nombre del contenedor]* 